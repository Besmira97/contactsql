
import java.sql.*;

public class Create {

    int parent_id = 0;

    private Connection conn;
    Create(Connection conn){
        this.conn = conn;
    }

    public int getParent_id(){
        return parent_id;
    }


    public void createPerson(String[] person_arguments, int[] numbers,String[] type, String[] mails, String[] type_mail,String[] relations){

        try {
            conn.setAutoCommit(false);

            String insertPerson = "INSERT INTO person(first_name,last_name,birth_date,home_adress) VALUES (?,?,?,?)";

            PreparedStatement statement = conn.prepareStatement(insertPerson, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1,person_arguments[0]);
            statement.setString(2,person_arguments[1]);
            statement.setDate(3,java.sql.Date.valueOf(person_arguments[2]));
            statement.setString(4, person_arguments[3]);

            statement.executeUpdate();

            this.conn.commit();
            ResultSet rs = statement.getGeneratedKeys();

            if(rs.next()){
                parent_id = rs.getInt(1);
            }

            createContactNumber(numbers,parent_id,type);
            createMail(mails,parent_id,type_mail);
            createFamily(relations,parent_id);

            rs.close();
            statement.close();
            conn.commit();

            System.out.println("Person created");

        }
        catch(SQLException e){

            try {
                this.conn.rollback();
                e.printStackTrace();
            }
            catch (SQLException e2){
                e2.printStackTrace();
            }
        }

    }

    public void createContactNumber(int[] numbers, int parent_id_input, String[] type_number){

        try {

            conn.setAutoCommit(false);

            String insertNumber = "INSERT INTO contact_number(number, parent_id, number_type) VALUES (?,?,?)";
            PreparedStatement phone_statement = conn.prepareStatement(insertNumber);

            for(int i=0; i<numbers.length;i++){

                phone_statement.setInt(1, numbers[i]);
                phone_statement.setInt(2, parent_id_input);
                phone_statement.setString(3,type_number[i]);
                phone_statement.executeUpdate();
            }

            this.conn.commit();

            System.out.println("Contact number created");
        }

        catch (SQLException e){
            try {
                this.conn.rollback();
                e.printStackTrace();
            }
            catch (SQLException e3){
                e3.printStackTrace();
            }
        }
    }

    public void createMail(String[] mails, int parent_id_input, String[] type_mail){


        try {
            conn.setAutoCommit(false);
            String insertMail = "INSERT INTO mail(e_mail, parent_id,mail_type) VALUES(?,?,?)";
            PreparedStatement mail_statement = conn.prepareStatement(insertMail);

            for(int i=0;i<mails.length;i++){
                mail_statement.setString(1,mails[i]);
                mail_statement.setInt(2,parent_id_input);
                mail_statement.setString(3,type_mail[i]);
                mail_statement.executeUpdate();
            }
            this.conn.commit();
            System.out.println("Mail created");
        }

        catch (SQLException e){
            try {
                this.conn.rollback();
                e.printStackTrace();
            }
            catch (SQLException e2){
                e2.printStackTrace();
            }
        }
    }

    public void createFamily(String[] relations, int parent_id_input){


        try {
            conn.setAutoCommit(false);
            String insertFamily = "INSERT INTO family(relation, parent_id) VALUES(?,?)";
            PreparedStatement family_statement = conn.prepareStatement(insertFamily);

            for(String relation: relations){
                family_statement.setString(1,relation);
                family_statement.setInt(2,parent_id_input);
                family_statement.executeUpdate();
            }
            this.conn.commit();
            System.out.println("Family created");
        }

        catch (SQLException e){
            try {
                this.conn.rollback();
                e.printStackTrace();
            }
            catch (SQLException e2){
                e2.printStackTrace();
            }
        }
    }

}
