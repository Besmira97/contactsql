import java.sql.*;

public class Read {
    private Connection connection;
    private ResultSet res;

    Read(Connection connection){
        this.connection = connection;
    }

    public boolean Read_by_First_Name(String first_name){
        String string_query = "SELECT *"+
                "FROM person p LEFT OUTER JOIN contact_number c ON c.parent_id = p.person_id " +
                "LEFT OUTER JOIN mail m ON m.parent_id = p.person_id " +
                "LEFT OUTER JOIN family f ON f.parent_ID = p.person_id " +
                "WHERE p.first_name =" +
                "'"  + first_name + "'" +
                "UNION "+

                "SELECT *"+
                "FROM person p RIGHT OUTER JOIN contact_number c ON c.parent_id = p.person_id " +
                "RIGHT OUTER JOIN mail m ON m.parent_id = p.person_id " +
                "RIGHT OUTER JOIN family f ON f.parent_ID = p.person_id " +
                "WHERE p.first_name =" +
                "'"  + first_name + "'";
        try {
            Statement stmt = this.connection.createStatement();
            this.res = stmt.executeQuery(string_query);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }


    }
    public boolean Read_by_Surname(String surname){
        String string_query = "SELECT *"+
                "FROM person p LEFT OUTER JOIN contact_number c ON c.parent_id = p.person_id " +
                "LEFT OUTER JOIN mail m ON m.parent_id = p.person_id " +
                "LEFT OUTER JOIN family f ON f.parent_ID = p.person_id " +
                "WHERE p.last_name =" +
                "'"  + surname + "'" +
                "UNION "+

                "SELECT *"+
                "FROM person p RIGHT OUTER JOIN contact_number c ON c.parent_id = p.person_id " +
                "RIGHT OUTER JOIN mail m ON m.parent_id = p.person_id " +
                "RIGHT OUTER JOIN family f ON f.parent_ID = p.person_id " +
                "WHERE p.last_name =" +
                "'"  + surname + "'";

        try {
            Statement stmt = this.connection.createStatement();
            this.res = stmt.executeQuery(string_query);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }

    }
    public boolean Read_by_Phone(int phone){
        String string_query = "SELECT *"+
                "FROM person p LEFT OUTER JOIN contact_number c ON c.parent_id = p.person_id " +
                "LEFT OUTER JOIN mail m ON m.parent_id = p.person_id " +
                "LEFT OUTER JOIN family f ON f.parent_ID = p.person_id " +
                "WHERE c.number =" +
                "'"  + phone + "'" +
                "UNION "+

                "SELECT *"+
                "FROM person p RIGHT OUTER JOIN contact_number c ON c.parent_id = p.person_id " +
                "RIGHT OUTER JOIN mail m ON m.parent_id = p.person_id " +
                "RIGHT OUTER JOIN family f ON f.parent_ID = p.person_id " +
                "WHERE c.number =" +
                "'"  + phone + "'";
        try {
            Statement stmt = this.connection.createStatement();
            this.res = stmt.executeQuery(string_query);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public ResultSet getRes() {
        return this.res;
    }
    public void print_res(){
        try {
            ResultSet resultSet = this.res;
            ResultSetMetaData rsmd = resultSet.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (resultSet.next()) {
                for (int i = 1; i <= columnsNumber; i++) {
                    if (i > 1) System.out.print(",  ");
                    String columnValue = resultSet.getString(i);
                    System.out.print(columnValue + " " + rsmd.getColumnName(i));
                }
                System.out.println("");
            }
        }catch( SQLException e){
            e.printStackTrace();
        }
    }
}
