import com.sun.corba.se.spi.protocol.InitialServerRequestDispatcher;

import jdk.nashorn.internal.ir.CatchNode;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Scanner;

public class Scan_Logic {
    private Scanner in;
    private Connection conn;
    private Delete delete;
    private Read read;
    private Update update;

    Scan_Logic(Connection conn) {
        in = new Scanner(System.in);
        this.conn = conn;
        this.delete = new Delete(conn);
        this.read = new Read(conn);
        this.update = new Update(conn);

    }

    // public method to start scans
    public void Start_Scans() {
        Initial_Scan();
    }

    // loopback scan
    void Initial_Scan() {
        System.out.println("Select one of the following options (1-5)");
        System.out.println("1: Create");
        System.out.println("2: Read");
        System.out.println("3: Update");
        System.out.println("4: Delete");
        System.out.println("5: Exit");

        String choice = in.nextLine();
        int option = Integer.parseInt(choice);
        switch (option) {
        case 1:
            CreateScan();
            break;
        case 2:
            ReadScan();
            break;
        case 3:
            UpdateScan();
            break;
        case 4:
            DeleteScan();
            break;
        case 5:
            System.exit(0);
        }

    }

    // scan to handle create
    private void CreateScan() {

        Create create = new Create(conn);
        System.out.println("Select one of the following options (1-5)");
        System.out.println("1: Person");
        System.out.println("2: Contact Number");
        System.out.println("3: Mail");
        System.out.println("4: Family");

        System.out.println("5: Back");

        String choice = in.nextLine();
        int option = Integer.parseInt(choice);
        switch (option) {
        case 1:
            System.out.println("Enter first_name,last_name,birth_date(YYYY-MM-DD),home_adress separated by comma");
            String s = in.nextLine();
            String[] person_arguments = s.split(",");

            System.out.println("Enter numbers separated by space");
            String number = in.nextLine();
            String[] numbers = number.split(" ");
            int[] numbers_int = Arrays.asList(numbers).stream().mapToInt(Integer::parseInt).toArray();

            System.out.println("Enter Number type separated by space");
            String number_type = in.nextLine();
            String[] number_types = number_type.split(" ");

            System.out.println("Enter e-mails separeted by space");
            String e_mail = in.nextLine();
            String[] e_mails = e_mail.split(" ");

            System.out.println("Enter e_mail type separated by space");
            String e_mail_type = in.nextLine();
            String[] e_mail_types = e_mail_type.split(" ");

            System.out.println("Enter relations separated by space");
            String relation = in.nextLine();
            String[] relations = relation.split(" ");

            create.createPerson(person_arguments, numbers_int, number_types, e_mails, e_mail_types, relations);

            break;

        case 2:

            System.out.println("Enter number(s) separeted by space");
            String number1 = in.nextLine();
            String[] numbers1 = number1.split(" ");
            int[] numbers1_int = Arrays.asList(numbers1).stream().mapToInt(Integer::parseInt).toArray();

            System.out.println("Enter parent ID");
            int person_id_n = Integer.parseInt(in.nextLine());

            System.out.println("Enter number(s) type(s) separated by space");
            String number_type1 = in.nextLine();
            String[] number_types1 = number_type1.split(" ");

            create.createContactNumber(numbers1_int, person_id_n, number_types1);

            break;
        case 3:

            System.out.println("Enter e-mails separeted by space");
            String e_mail1 = in.nextLine();
            String[] e_mails1 = e_mail1.split(" ");

            System.out.println("Enter parent ID");
            int person_id_M = Integer.parseInt(in.nextLine());

            System.out.println("Enter e_mail type separated by space");
            String e_mail_type1 = in.nextLine();
            String[] e_mail_types1 = e_mail_type1.split(" ");

            create.createMail(e_mails1, person_id_M, e_mail_types1);

            break;
        case 4:

            System.out.println("Enter relation(s) separeted by space");
            String relation1 = in.nextLine();
            String[] relations1 = relation1.split(" ");

            System.out.println("Enter parent ID");
            int person_id_F = Integer.parseInt(in.nextLine());

            create.createFamily(relations1, person_id_F);
            break;

        case 5:
            break;
        }
        Initial_Scan();

    }

    // scan to handle read
    private void ReadScan() {
        System.out.println("Select one of the following options to search by (1-4)");
        System.out.println("1: First Name");
        System.out.println("2: Surname");
        System.out.println("3: Phone number");
        System.out.println("4: Back");
        String input = in.nextLine();
        int choice = Integer.parseInt(input);

        switch (choice) {
        case 1:
            System.out.println("Enter First Name");
            input = in.nextLine();
            read.Read_by_First_Name(input);
            read.print_res();
            break;
        case 2:
            System.out.println("Enter Surname");
            input = in.nextLine();
            read.Read_by_Surname(input);
            read.print_res();
            break;
        case 3:
            System.out.println("Enter Phone number");
            input = in.nextLine();
            int num = Integer.parseInt(input);
            read.Read_by_Phone(num);
            read.print_res();
            break;
        case 4:
            break;
        }

        Initial_Scan();
    }

    // scan to handle update
    private void UpdateScan() {

        System.out.println("Select one of the following options (1-5)");
        System.out.println("1: Person");
        System.out.println("2: Contact Number");
        System.out.println("3: Mail");
        System.out.println("4: Family");
        System.out.println("5: Back");

        String choice = in.nextLine();
        int option = Integer.parseInt(choice);

        String prompt;
        String input;
        String[] fields;

        switch (option) {
        case 1:
            prompt = "To update [person] enter input in this format: [person_id,first_name,last_name,home_address,birth_date]"
                    + "\nBrackets not needed. Seperate each field with a comma [,]. If field does not require change leave field position empty, (no whitespace), eg. [person_id,first_name,,,], this will leave [last_name home_address birth_date] unchanged"
                    + "\n[person_id] needs to be present in [person]."
                    + "\n[birth_date] needs to have format [yyyy-mm-dd]"
                    + "\nAs the system comma seperates input, no field is allowed to contain [,]";

            System.out.println(prompt);
            input = in.nextLine();
            fields = input.split(",");

            int person_id;
            String[] person_args;

            try {
                person_id = Integer.parseInt(fields[0]);
                person_args = Arrays.copyOfRange(fields, 1, fields.length);
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
                break;
            }

            update.updatePerson(person_id, person_args);
            break;
        case 2:
            prompt = "To update [contact number] enter input in this format: [number_id,number,number_type]"
                    + "\nBrackets not needed. Seperate each field with a comma [,]."
                    + "\nIf field does not require change leave field position empty, (no whitespace), eg. [number_id,number,], this will leave [number_type] unchanged"
                    + "\n[number_id] needs to be present in [contact number]."
                    + "\n[number] is purly of type integer, not other characters."
                    + "\nAs the system comma seperates input, no field is allowed to contain [,]";

            System.out.println(prompt);
            input = in.nextLine();
            fields = input.split(",");

            int number_id;
            int number;
            String numberType;

            try {
                number_id = Integer.parseInt(fields[0]);
                number = Integer.parseInt(fields[1]);
                numberType = fields[2];
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
                break;
            }

            update.updateNumber(number_id, number, numberType);

            break;
        case 3:
            prompt = "To update [mail] enter input in this format: [mail_id,mail,mail_type]"
                    + "\nBrackets not needed. Seperate each field with a comma [,]."
                    + "\nIf field does not require change leave field position empty, (no whitespace), eg. [mail_id,mail,], this will leave [mail_type] unchanged"
                    + "\n[mail_id] needs to be present in [mail]."
                    + "\nAs the system comma seperates input, no field is allowed to contain [,]";

            System.out.println(prompt);
            input = in.nextLine();
            fields = input.split(",");

            int mail_id;
            String[] mail_args;

            try {
                mail_id = Integer.parseInt(fields[0]);
                mail_args = Arrays.copyOfRange(fields, 1, fields.length);
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
                break;
            }

            update.updateEmail(mail_id, mail_args);

            break;
        case 4:
            prompt = "To update [family] enter input in this format: [family_id,family_relation]"
                    + "\nBrackets not needed. Seperate each field with a comma [,]."
                    + "\n[family_id] needs to be present in [family]."
                    + "\nAs the system comma seperates input, no field is allowed to contain [,]";

            System.out.println(prompt);
            input = in.nextLine();
            fields = input.split(",");

            int family_id;
            String family_relation;

            try {
                family_id = Integer.parseInt(fields[0]);
                family_relation = fields[1];
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
                break;
            }

            update.updateFamily(family_id, family_relation);

            break;
        case 5:
            break;
        }
        Initial_Scan();
    }

    // scan to handle delete
    private void DeleteScan() {

        System.out.println("Select one of the following options (1-5)");
        System.out.println("1: Person");
        System.out.println("2: Contact Number");
        System.out.println("3: Mail");
        System.out.println("4: Family");
        System.out.println("5: Back");

        String choice = in.nextLine();
        int option = Integer.parseInt(choice);
        switch (option) {
        case 1:
            System.out.println("Enter person ID");
            int person_id = Integer.parseInt(in.nextLine());
            delete.deletePerson(person_id);
            break;
        case 2:
            System.out.println("Enter contact number ID");
            int phone_id = Integer.parseInt(in.nextLine());
            delete.deleteContactNumber(phone_id);
            break;
        case 3:
            System.out.println("Enter mail ID");
            int mail_id = Integer.parseInt(in.nextLine());
            delete.deleteMail(mail_id);
            break;
        case 4:
            System.out.println("Enter family ID");
            int family_id = Integer.parseInt(in.nextLine());
            delete.deleteFamily(family_id);
            break;
        case 5:
            break;
        }
        Initial_Scan();
    }

}
