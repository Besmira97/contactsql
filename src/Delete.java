import javax.swing.plaf.nimbus.State;
import java.sql.*;

public class Delete {

    private Connection conn;

    Delete(Connection conn){
        this.conn = conn;
    }

    public boolean deletePerson(int person_id_arg){

        try{
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            String deletePerson = "delete from person where person_id='"+person_id_arg+"'";
            int rows_affected = stmt.executeUpdate(deletePerson);

            if(rows_affected==0){
                System.out.println("Nothing deleted");
                return false;
            }

            this.conn.commit();
        }

        catch(SQLException e){
            e.printStackTrace();
        }
        System.out.println("Successfully  deleted person");
        return true;
    }

    public boolean deleteContactNumber(int phone_id_arg){
        try{
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            String deleteContactNumber = "delete from contact_number where phone_id='"+phone_id_arg+"'";
            int rows_affected = stmt.executeUpdate(deleteContactNumber);
            this.conn.commit();

            if(rows_affected ==0){
                System.out.println("Nothing deleted");
                return false;
            }
        }
        catch (SQLException e){
            e.printStackTrace();
            return false;
        }
        System.out.println("Successfully  deleted contact number");
        return true;
    }

    public boolean deleteMail(int mail_id_arg){
        try{
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            String deleteMail = "delete from mail where mail_id='"+mail_id_arg+"'";
            int rows_affected = stmt.executeUpdate(deleteMail);
            this.conn.commit();

            if(rows_affected ==0){
                System.out.println("Nothing deleted");
                return false;
            }
        }
        catch (SQLException e){
            e.printStackTrace();
            return false;
        }
        System.out.println("Successfully  deleted mail");
        return true;
    }

    public boolean deleteFamily(int family_id_arg){
        try{
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            String deleteFamily = "delete from family where family_id='"+family_id_arg+"'";
            int rows_affected = stmt.executeUpdate(deleteFamily);
            this.conn.commit();

            if(rows_affected ==0){
                System.out.println("Nothing deleted");
                return false;
            }
        }
        catch (SQLException e){
            e.printStackTrace();
            return false;
        }
        System.out.println("Successfully  deleted family");
        return true;
    }

}
