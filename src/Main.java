import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class Main {

    public static void main(String[] args) {
        Connector connection = new Connector();
        connection.connect();

        Scan_Logic scan = new Scan_Logic(connection.get_connection());
        scan.Start_Scans();
    }
}
