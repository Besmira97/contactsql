import java.sql.DriverManager;
import java.sql.Connection;

public class Connector {
    public String Login;
    public String MotDePasse;
    private boolean Logged = false;
    private Connection connection;
    private boolean connected;

    public void connect(){
        try {
            String driverName = "com.mysql.cj.jdbc.Driver";
            Class.forName(driverName); // here is the ClassNotFoundException

            String serverName = "remotemysql.com";
            String mydatabase = "k6wDgGCWbV";
            String url = "jdbc:mysql://" + serverName + "/" + mydatabase;

            String username = "k6wDgGCWbV";
            String password = "0Z0TSCbtQj";
            this.connection = DriverManager.getConnection(url, username, password);
            this.connected = true;
            System.out.println("Connection Established.");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void close(){
        try{
            this.connection.close();
            this.connected = false;
            System.out.println("Connection Closed!");
        }catch(Exception e){
            e.printStackTrace();
        }

    }
    public Connection get_connection(){
        if(connected){
            return this.connection;
        }
        return null;
    }
}

