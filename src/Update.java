import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

//int person_id, Person_args[], number_args[], String numbertype, mail_args[], family[]


public class Update {
    private Connection conn;
    private String[] table_name = {"person", "contact_number", "mail", "family"};

    Update(Connection conn) {
        this.conn = conn;
    }


    // what is a date format? ask Alexander
    public boolean updatePerson(int person_id,
                                String[] person_args) {
        String updatePersonPositionSQl = updatePersonPositionSQl(person_args);
        if (updatePersonPositionSQl.isEmpty()) {
            return false;
        }

        System.out.println("updatePersonPositionSQl: " + updatePersonPositionSQl);

        try {
            return pushPersonpstmt(updatePersonPositionSQl, person_id, person_args);
        } catch (Exception e) {
            return false;
        }
    }

    private boolean pushPersonpstmt(String updatePersonPositionSQl, int person_id, String[] person_args) throws SQLException {
        boolean noError = false;

        PreparedStatement pstmt = conn.prepareStatement(updatePersonPositionSQl);

        int count = 1;
        int date_check = 1;
        for (String s : person_args) {
            if (!s.isEmpty()) {
                if (date_check == person_args.length) {
                    pstmt.setDate(count, java.sql.Date.valueOf(s));
                    count++;
                } else {
                    pstmt.setString(count, s);
                    count++;
                }

            }

            date_check++;
        }

        pstmt.setInt(count, person_id);

        boolean autoCommit = conn.getAutoCommit();
        try {
            conn.setAutoCommit(false);
            pstmt.executeUpdate();
            conn.commit();

            noError = true;
        } catch (SQLException sql) {
            try {
                conn.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            noError = false;
        } finally {
            try {
                conn.setAutoCommit(autoCommit);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return noError;
    }

    private String updatePersonPositionSQl(String[] person_args) {
        boolean nothingToChange = true;
        String updateQuery = "UPDATE "+ table_name[0] +" SET ";
        String[] field = {"first_name", "last_name", "home_adress", "birth_date"};

        int count = 0;
        for (String s : person_args) {
            if(!s.isEmpty()) {
                if(!nothingToChange) {
                    updateQuery += ", ";
                }
                updateQuery += field[count] + "=?";
                nothingToChange = false;
            }
            count++;
        }

        if (nothingToChange) {
            return "";
        }

        updateQuery += " WHERE person_id=?";

        return updateQuery;
    }


    /* input
     * int person_id : parent ID
     * int number : if -1 no update needed, else updated with given number
     * String numberType : if empty string no change, else updated with given string
     */
    public boolean updateNumber(int number_id,
                                int number,
                                String numberType) {

        String updateNumberPositionSql = updateNumberPositionSQl(number, numberType);
        if (updateNumberPositionSql.isEmpty()) {
            return false;
        }

        System.out.println("updateNumberPositionSql: " + updateNumberPositionSql);

        try {
            return pushNumberpstmt(updateNumberPositionSql, number_id, number, numberType);
        } catch (Exception e) {
            return false;
        }
    }

    private boolean pushNumberpstmt(String updateNumberPositionSql, int number_id, int number, String numberType) throws SQLException {
        boolean noError = false;
        PreparedStatement pstmt = conn.prepareStatement(updateNumberPositionSql);

        int count = 1;

        // number update
        if (number != -1) {
            pstmt.setInt(count, number);
            count++;
        }

        // number type update
        if (!numberType.isEmpty()) {
            pstmt.setString(count, numberType);
            count++;
        }

        // where statement
        pstmt.setInt(count, number_id);

        boolean autoCommit = conn.getAutoCommit();
        try {
            conn.setAutoCommit(false);
            pstmt.executeUpdate();
            conn.commit();

            noError = true;
        } catch (SQLException sql) {
            try {
                conn.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            noError = false;
        } finally {
            try {
                conn.setAutoCommit(autoCommit);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return noError;
    }

    private String updateNumberPositionSQl(int number, String numberType) {
        boolean nothingToChange = true;
        String updateQuery = "UPDATE "+ table_name[1] +" SET ";
        String[] field = {"number", "number_type"};

        // number update
        if (number != -1) {
            updateQuery += field[0] + "=?";
            nothingToChange = false;
        }

        // number type update
        if (!numberType.isEmpty()) {
            updateQuery += ", " + field[1] + "=?";
            nothingToChange = false;
        }

        if (nothingToChange) {
            return "";
        }

        updateQuery += " WHERE phone_id=?";
        return updateQuery;
    }


    /* expected input
    * int email_id : unique id
    * String[] email : [email address, email type]
    */
    public boolean updateEmail(int email_id,
                               String[] email) {
        String updateEmailPositionSql = updateEmailPositionSql(email);
        if (updateEmailPositionSql.isEmpty()) {
            return false;
        }

        System.out.println("updateEmailPositionSql: " + updateEmailPositionSql);

        try {
            return pushEmailpstmt(updateEmailPositionSql, email_id, email);
        } catch (Exception e) {
            System.out.println("Last catch, in updateEmail(): Line ~215");
            e.printStackTrace();
            return false;
        }
    }

    private boolean pushEmailpstmt(String updateNumberPositionSql, int email_id, String[] email) throws SQLException {
        boolean noError = false;
        PreparedStatement pstmt = conn.prepareStatement(updateNumberPositionSql);

        int count = 1;
        for (String s : email) {
            if(!s.isEmpty()) {
                pstmt.setString(count,s);
                count++;
            }
        }

        // where statement
        pstmt.setInt(count, email_id);

        boolean autoCommit = conn.getAutoCommit();
        try {
            conn.setAutoCommit(false);
            pstmt.executeUpdate();
            conn.commit();

            noError = true;
        } catch (SQLException sql) {
            try {
                conn.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            noError = false;
        } finally {
            try {
                conn.setAutoCommit(autoCommit);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return noError;
    }

    private String updateEmailPositionSql(String[] email) {
        boolean nothingToChange = true;
        String updateQuery = "UPDATE "+ table_name[2] +" SET ";
        String[] field = {"e_mail", "mail_type"};

        int count = 0;

        for (String s : email) {
            if(!s.isEmpty()) {
                if(!nothingToChange) {
                    updateQuery += ", ";
                }
                updateQuery += field[count] + "=?";
                nothingToChange = false;
            }
            count++;
        }

        if (nothingToChange) {
            return "";
        }

        updateQuery += " WHERE mail_id=?";
        return updateQuery;
    }

    /* expected input
    * int family_id : unique id
    * String family : relation type
    */
    public boolean updateFamily(int family_id,
                                String family) {
        String updateFamilyPositionSql = updateFamilyPositionSql(family);
        if (updateFamilyPositionSql.isEmpty()) {
            return false;
        }

        System.out.println("updateFamilyPositionSql: " + updateFamilyPositionSql);

        try {
            return pushFamilypstmt(updateFamilyPositionSql, family_id, family);
        } catch (Exception e) {
            return false;
        }
    }

    private boolean pushFamilypstmt(String updateNumberPositionSql, int family_id, String family) throws SQLException {
        boolean noError = false;
        PreparedStatement pstmt = conn.prepareStatement(updateNumberPositionSql);

        int count = 1;
        pstmt.setString(count, family);
        count++;

        // where statement
        pstmt.setInt(count, family_id);

        boolean autoCommit = conn.getAutoCommit();
        try {
            conn.setAutoCommit(false);
            pstmt.executeUpdate();
            conn.commit();

            noError = true;
        } catch (SQLException sql) {
            try {
                conn.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            noError = false;
        } finally {
            try {
                conn.setAutoCommit(autoCommit);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return noError;
    }

    private String updateFamilyPositionSql(String family) {
        String updateQuery = "UPDATE "+ table_name[3] +" SET ";
        String[] field = {"relation"};

        if (!family.isEmpty()) {
            updateQuery += field[0] + "=?";
            updateQuery += " WHERE family_id=?";
            return updateQuery;
        }else {
            return "";
        }
    }
}

